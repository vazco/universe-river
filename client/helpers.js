'use strict';

UniRiver.addHelper('UniRiverActivity', 'UniRiverMessage', function () {
    if (this.action) {
        return Template[UniPlugin.getTemplate('UniRiverMessage-' + this.action)] ||
            Template[UniPlugin.getTemplate('UniRiverMessage-default')] ||
            null;
    }
    return null;
});
UniRiver.addHelper('UniRiverActivity', 'timeAgo', function () {
    var date = this.createdAt;
    if (!_.isDate(date)) {
        return;
    }
    if (moment) {
        return moment(date).fromNow();
    }
    return date.toDateString();
});

UniRiver.addHelper('UniRiver', 'getActivities', function (query, options) {
    query = _.isObject(query) ? query : {};
    options = _(options || {}).defaults({
        sort: {
            createdAt: -1
        }
    });

    return UniRiver.Activities.find(query, options);
});
