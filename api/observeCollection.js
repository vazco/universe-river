'use strict';

/**
 * Set collection hooks to add UniRiver activities
 * @param collection
 * @param options
 */
UniRiver.observeCollection = function (collection, options) {
    check(collection, Mongo.Collection);
    check(options, Object);

    _.chain(options)
        .pick('insert', 'update', 'remove')
        .each(function (arrayOfParams, action) {
            if (_.isString(arrayOfParams)) {
                // Support for simple syntax
                arrayOfParams = [{action: arrayOfParams}];

            } else if (!_.isArray(arrayOfParams)) {
                // Support for one object instead of array
                arrayOfParams = [arrayOfParams];
            }

            _(arrayOfParams).each(function (params) {
                _.defaults(params, options.defaults);

                collection.after[action](function (userId, doc) {
                    if (_.isFunction(params.predicate)) {
                        if (!params.predicate.apply(this, arguments)) {
                            //predicate returns false, skip execution
                            return;
                        }
                    }

                    if (_.isArray(params.triggeringFields) && action === 'update') {
                        var self = this;
                        var changeDetected = _.some(params.triggeringFields, function (fieldName) {
                            return !EJSON.equals(UniUtils.get(self.previous, fieldName), UniUtils.get(doc, fieldName));
                        });

                        if (!changeDetected) {
                            return;
                        }
                    }

                    var object = {}; //activity object, the "o"

                    if (_.isObject(params.fields)) {
                        // When fields is set, map field properties
                        _(params.fields).each(function (value, key) {
                            if (_.isFunction(value)) {
                                // Function was provided, execute it
                                object[key] = value(doc);
                            } else {
                                // Simple map value using Vazco.get
                                object[key] = Vazco.get(doc, value);
                            }
                        });
                    }

                    // Now map regular properties
                    var specialProps = ['action', 'fields', 'predicate',
                                        'callback', 'triggeringFields'];
                    var definedProps = Object.keys(object);
                    object = _(object).extend(
                        _(params).omit(specialProps), _(doc).omit(definedProps)
                    );

                    UniRiver.addActivity(params.action, object, params.callback || function () {});
                });
            });
        });
};
