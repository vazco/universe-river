'use strict';

/**
 * Adds new item to the river
 * @param actionName name of the registered action
 * @param object river object
 */
UniRiver.addActivity = function (actionName, object, callback) {
    check(actionName, String);
    check(object, Object);
    Meteor.call('UniRiverAddActivity', actionName, object, callback);
};
