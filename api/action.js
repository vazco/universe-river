'use strict';

/**
 *
 * @param name of the new action
 * @param options
 */

var _actions = {};

UniRiver.registerAction = function (name, options) {
    check(name, String);
    check(options, Object);

    options.name = name;

    _actions[name] = options;
};

UniRiver.getAction = function (name) {
    return _actions[name];
};

UniRiver.listActions = function () {
    return _.keys(_actions);
};

UniRiver.addObjectSchema = function(name, schema){
    var action = this.getAction(name);
    if(!action){
        return false;
    }
    check(schema, SimpleSchema);

    action.schema = new SimpleSchema([action.schema, schema]);
    return true;
};