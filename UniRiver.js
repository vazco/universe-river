'use strict';

/* global UniRiver: true */

UniRiver = new UniPlugin('UniRiver');

UniRiver.transformActor = function (actor) {
    return {
        _id: actor._id,
        name: actor.username
    };
};
