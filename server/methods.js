'use strict';

UniRiver.addMethods({
    UniRiverAddActivity: function (name, object, actor) {
        check(name, String);
        check(object, Object);

        actor = actor || Meteor.user();

        if (!actor) {
            throw new Meteor.Error(302, 'Only logged in users can perform actions');
        }

        var action = UniRiver.getAction(name);
        if (!action) {
            throw new Meteor.Error(500, 'Invalid action');
        }

        var debug;
        var ssCtx;
        var errMsg;
        if (action.schema) {
            debug = SimpleSchema.debug;
            SimpleSchema.debug = false;
            action.schema.clean(object);
            SimpleSchema.debug = debug;
            ssCtx = action.schema.newContext();
            if (!ssCtx.validate(object)) {
                errMsg = ssCtx.invalidKeys().map(function (e) {
                    return e.name;
                }).join(', ');
                throw new Meteor.Error(500, 'River object validation error in ' + errMsg);
            }
        }

        var doc = {
            actor: UniRiver.transformActor(actor),
            action: name,
            o: object,
            createdAt: new Date()
        };

        return UniRiver.Activities.insert(doc);
    }
});
