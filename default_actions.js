'use strict';

var defaultSchema = new SimpleSchema({
    _id: {
        type: String
    },
    type: {
        type: String,
        optional: true
    },
    title: {
        type: String
    },
    route: {
        type: String,
        optional: true
    }
});

UniRiver.registerAction('created', {schema: defaultSchema});
UniRiver.registerAction('added', {schema: defaultSchema});
UniRiver.registerAction('updated', {schema: defaultSchema});
UniRiver.registerAction('edited', {schema: defaultSchema});
UniRiver.registerAction('deleted', {schema: defaultSchema});
UniRiver.registerAction('removed', {schema: defaultSchema});
