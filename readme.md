# Activity River for Universe

## Idea

Idea is loosely based on [Facebook Open Graph](https://developers.facebook.com/docs/opengraph/overview/)

A typical activity consists of three elements: an actor (user), action, and activity object.

With these elements you can create actions like **Mike created post Hello**, where:

* **Mike** is an actor, linked to to his profile
* **created** is an action
* **post Hello** is activity object data, where post is a type and Hello is title with a link

### Glossary

#### Action

Action is a defined object, with custom logic to handle each activity.

Example actions are:

* created
* updated
* deleted
* like
* read

Plugin ships with basic actions, and any other Universe plugin or project itself can add own actions using `UniRiver.registerAction`.

#### Activity Object

Activity object data, contains fields defined in schema registered with action.

#### Actor

Actor object, contains:

* `userId`: String
* `name`: String

At this time, actor is filled by `UniRiver` automatically.

#### Activity

Activity is a performed action, represented as a document in `UniRiver.Activities` collection.


### Subscribing to data and displaying river

Right now all subscription-related work needs to be done in the project part of code,
the same goes for securing the content. This may change in the future.

To display river, just embed `UniRiver` template:

    {{> UniRiver}}

If you want to narrow river results, you can pass mongo selector as query object parameter

    {{> UniRiver query=riverQuery}}

You should also overwrite default activity view to customize it:

    UniPlugin.setTemplate('UniRiverActivity', 'foyerActivity');

There are few helpers on the `UniRiverActivity` template:

* `{{> UniRiverMessage}}` returns customized message for each action. This is not a template, is
* `{{timeAgo}}` shows how much time passed since activity was added.

In the context you have available:

* `actor` - actor object, usually just pass it to `UniRiverActor` view.
* `action` - name of the action
* `o` - activity object data
* `createdAt` - date object when activity was added to river



## API Reference


### UniRiver.registerAction(`actionName:String`, `options:Object`) *[Server]*

Register new action type, or overwrite completely existing action.

Options:

* `schema` - a Simple Schema object, by which every activity object will be validated against

Example:

    UniRiver.registerAction('watch', {
        schema: new SimpleSchema({
            _id: {
                type: String
            },
            title: {
                type: String
            },
            route: {
                type: String
            }
        })
    });


Additionally each action can have customized message view.
Just register a template in Universe under name `UniRiverMessage-actionName`.

Example of such template:

    <template name="UniRiverMessage-watch">
        {{> uniDynamic 'UniRiverActor'}} watched movie {{o.title}}
    </template>

Where in context you have the same fields as in `UniRiverActivity`.



### UniRiver.getAction(`actionName:String`) *[Server]*

Helper, returns action object by reference (this may change in the future).

Example:

    UniRiver.getAction('watch');

### UniRiver.listActions() *[Server]*

Helper, returns array with all registered actions.

Example:

    UniRiver.listActions();

### UniRiver.addObjectSchema(`actionName:String`, `schema:SimpleSchema`) *[Server]*

Used to extend object schema for an action. You need to provide a SimpleSchema object.

Example:

    UniRiver.addObjectSchema('watch', new SimpleSchema({
        imdb_id: {
            type: String,
            optional: true
        }
    }));

### UniRiver.observeCollection(`collection:Mongo.Collection`, `options:Object`) *[Server]*

Add hooks on collection, using [matb33:collection-hooks](https://github.com/matb33/meteor-collection-hooks).

Valid options:

* `insert`, `update`, `remove` - settings for each collection hook. You can provide either:
    + Object (default) - object with settings (see below)
    + String (simple syntax) - string with action name, if you don't need other settings
    + Array - array of objects with settings

* `defaults` - default options on each of above hooks so you don't have to copy/paste them

Each hook settings are:

* `action` - action name

* `fields` - key-value hash to map field from document (value) to activity object (key).
If left empty, will copy all properties from document, which will be sanitized by SimpleSchema later in the process.
You can also provide function that takes `doc` as a param and returns value

* `predicate` - a function that (when provided) must return `true` to add the activity.
This function is executed in the same context as collection hook itself, with the same arguments available.
Check out collection hooks doc for more info.

* `triggeringFields` (only for update) - array of fields names. The activity will be added only if change in at least one field is detected.

* `callback` - optional callback function passed down to `addActivity`.

* any other valid schema key on activity object


Example:

    UniRiver.observeCollection(Colls.Movies, {
        insert: 'created', //simple syntax
        update: [ //array syntax
            {
                action: 'updated',
                predicate: function(userId, doc, fieldNames, modifier, options){
                    return !!doc.showInRiver;
                }
            },
            {
                action: 'liked',
                triggeringFields: ['likes']
            }
        ],
        remove: { //default syntax
            action: 'deleted',
            route: null //overwrite default value
        },
        defaults: {
            type: 'movie',
            route: 'movies.view',
            fields: {
                imdb_id: 'imdb.id' //map imdb.id from doc's subdocument to activity object imdb_id field
            }
        }
    });



### UniRiver.addActivity(`actionName:String`, `objectData:Object`, [`callback:Function`]) *[Anywhere]*

Main method. Used to add new Activity to the River.

* `actionName` must be a registered action
* `objectData` activity object data compatible with the action schema.
    For the default methods (`created`, `updated` etc.) you need to provide:
    * `_id` - doc id
    * `title` - doc display title
    * `type` - optional, doc type (usually a string human readable identifier)
    * `route` - optional, Iron.Router route name to link to it

* `callback` optional, standard Meteor method callback, return activity id on success or error on failure


Example:

    UniRiver.addActivity('watch', {
        _id: '52v2bF8GtxkpoLemt',
        title: 'Batman',
        route: 'movies.view',
        imdb_id: 'tt0372784'
    });


