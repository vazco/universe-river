Package.describe({
    name: 'vazco:universe-river',
    summary: 'Activity River for Universe',
    version: '0.0.7'
});

Package.onUse(function (api) {
    api.versionsFrom('1.0.1');

    api.use([
        'check',
        'templating',
        'underscore',
        'vazco:universe-core',
        'vazco:universe-core-plugin',
        'anti:i18n',
        'aldeed:simple-schema',
    ], ['client', 'server']);

    api.addFiles([
        'UniRiver.js',
        'collections.js',
        'api/activity.js',
    ]);

    api.addFiles([
        'server/methods.js',
        'api/action.js',
        'api/observeCollection.js',
        'default_actions.js'
    ], 'server');

    api.addFiles([
        'default_actions.html',
        'client/templates.html',
        'client/helpers.js'
    ], 'client');


    api.export('UniRiver');
});

