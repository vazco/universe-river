'use strict';
var mixins = UniCollection.mixins? [new UniCollection.mixins.PublishAccessMixin()]: undefined;
UniRiver.addCollection('Activities', {
    mixins: mixins,
    onInit: function (collection) {
        collection.Schema = new SimpleSchema({
            actor: {
                type: Object,
                blackbox: true
            },
            'actor._id': {
                type: String
            },
            action: {
                type: String
            },
            createdAt: {
                type: Date
            },
            o: {
                type: Object,
                blackbox: true
            }
        });

        collection.attachSchema(collection.Schema);

        collection.helpers({
            //@todo get user helper
        });

        collection.allow({
            insert: function () {
                return false;
            },
            update: function () {
                return false;
            },
            remove: function () {
                return false;
            }
        });
        
        if (_.isFunction(UniRiver.onInitCallback)) {
            UniRiver.onInitCallback.call(this, collection);
        }
  
    }
});
